import { MutationTree } from "vuex";
import { MainStateInterface } from "./state";

export const types = {
  SET_IMAGES_IDS: "SET_IMAGES_IDS"
};

const mutation: MutationTree<MainStateInterface> = {
  [types.SET_IMAGES_IDS](state, list) {
    state.imagesIds = list;
  }
};

export default mutation;
