import { GetterTree } from "vuex";
import { StateInterface } from "@/store";
import { MainStateInterface } from "./state";

const getters: GetterTree<MainStateInterface, StateInterface> = {
  getImagesIds: state => {
    return state.imagesIds;
  }
};

export default getters;
