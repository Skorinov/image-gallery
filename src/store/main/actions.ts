import { ActionTree } from "vuex";
import { StateInterface } from "../index";
import { MainStateInterface } from "./state";
import { types } from "@/store/main/mutations";

const actions: ActionTree<MainStateInterface, StateInterface> = {
  setImagesIds({ commit }, list: string[]) {
    commit(types.SET_IMAGES_IDS, list);
    return list;
  }
};

export default actions;
