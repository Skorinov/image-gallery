export interface MainStateInterface {
  imagesIds: string[];
}

const state: MainStateInterface = {
  imagesIds: []
};

export default state;
