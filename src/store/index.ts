import Vue from "vue";
import Vuex from "vuex";
import { MainStateInterface } from "@/store/main/state";
import mainModule from "@/store/main";

Vue.use(Vuex);

export interface StateInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  mainState: MainStateInterface;
}

export default new Vuex.Store({
  modules: {
    mainModule
  }
});
