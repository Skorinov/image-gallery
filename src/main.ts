import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import init from "../plugins/init.client";
import PortalVue from "portal-vue";

Vue.config.productionTip = false;

Vue.use(init);
Vue.use(PortalVue);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
