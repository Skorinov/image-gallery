import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import NetworkService from "../../services/network.service";
import AuthService from "../../services/auth.service";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "home",
    component: () => import("../views/Home.vue"),
    children: [
      {
        path: "/image/:imageId",
        name: "image",
        component: () => import("../components/ImageModal.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  try {
    const hasHeader: boolean = NetworkService.checkHeaders();
    if (!hasHeader) {
      const token: any = AuthService.getTokenFromLocalStorage();
      if (token) {
        AuthService.setToken(token);
      } else {
        await AuthService.login();
      }
      next();
    } else {
      next();
    }
  } catch (e) {
    console.log("error", e);
    next();
  }
});

export default router;
