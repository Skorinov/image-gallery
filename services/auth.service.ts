import NetworkService, { POST } from "./network.service";

export const TOKEN_KEY = "token";

class AuthService {
  public static async login() {
    try {
      NetworkService.removeHeader();
      const data: any = {
        apiKey: process.env.VUE_APP_API_KEY
      };
      const response = await POST("/auth", data, {}, true);
      if (response?.data) {
        AuthService.setToken(response.data?.token);
        AuthService.setTokenToLocalStorage(response.data?.token);
        return response.data;
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  public static setToken(token: string) {
    if (token) {
      NetworkService.setHeader({
        Authorization: `Bearer ${token}`
      });
    }
  }

  public static setTokenToLocalStorage(token: string) {
    if (token) {
      localStorage.setItem(TOKEN_KEY, token);
    }
  }

  public static getTokenFromLocalStorage() {
    const token: string | null = localStorage.getItem(TOKEN_KEY);
    return token;
  }
}

export default AuthService;
