import { GET } from "./network.service";

export interface ApiServiceInterface {
  getImages: (page?: number) => any;
  getImage: (id: string) => any;
}

class ApiService {
  public async getImages(page?: number) {
    try {
      const options: string = page ? `?page=${page}` : "";
      const result: any = await GET(`/images${options}`);
      if (result?.data) {
        return result.data;
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  public async getImage(id: string) {
    try {
      const result: any = await GET(`/images/${id}`);
      if (result?.data) {
        return result.data;
      }
      return null;
    } catch (e) {
      console.log(e);
      return null;
    }
  }
}

export default ApiService;
