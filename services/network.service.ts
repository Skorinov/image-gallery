import axios, { AxiosResponse } from "axios";
import AuthService from "./auth.service";

class NetworkService {
  private static instance: NetworkService;

  private constructor() {}

  public static init() {
    if (!NetworkService.instance) {
      NetworkService.instance = new NetworkService();
      axios.defaults.baseURL = process.env.VUE_APP_API_URL;
    }

    return NetworkService.instance;
  }

  public static checkHeaders() {
    const headers = axios.defaults.headers.common;
    return !!headers["Authorization"];
  }

  public static setHeader(headers: any) {
    axios.defaults.headers.common = {
      ...axios.defaults.headers.common,
      ...headers
    };
  }

  public static removeHeader() {
    axios.defaults.headers.common = {};
  }

  private static async resendAction(
    request: () => Promise<AxiosResponse<any>>
  ) {
    const newResponse: any = await AuthService.login();
    if (!newResponse?.token) {
      return null;
    } else {
      const res = await request().catch(_error => {
        console.log(_error);
      });
      return res;
    }
  }

  private static async handleRequest(
    request: () => Promise<AxiosResponse<any>>,
    isLogin = false
  ) {
    const hasHeaders = NetworkService.checkHeaders();

    if (!hasHeaders && !isLogin) {
      return false;
    } else {
      const response: any = await request().catch(async (error: any) => {
        const errorResponse: any = error?.response;
        if (errorResponse?.status === 401) {
          const res = await NetworkService.resendAction(request);
          return res;
        }
      });

      if (response?.status === 401) {
        const res = await NetworkService.resendAction(request);
        return res;
      } else {
        return response;
      }
    }
  }

  public static get(resource: string, options?: any) {
    const httpRequest = () => {
      return axios.get(resource, options || {});
    };
    return NetworkService.handleRequest(httpRequest);
  }

  public static post(
    resource: string,
    data: any,
    options?: any,
    isLogin = false
  ): any {
    const httpRequest = () => {
      return axios.post(resource, data, options || {});
    };

    return NetworkService.handleRequest(httpRequest, isLogin);
  }
}

export const GET = NetworkService.get;
export const POST = NetworkService.post;
export default NetworkService;
