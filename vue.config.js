process.env.VUE_APP_API_URL = "http://interview.agileengine.com";
process.env.VUE_APP_API_KEY = "23567b218376f79d9415";

module.exports = {
  lintOnSave: false,
  pages: {
    index: {
      entry: "src/main.ts",
      template: "public/index.html",
      filename: "index.html",
      title: "Image Gallery",
      chunks: ["chunk-vendors", "chunk-common", "index"]
    }
  },
  devServer: {
    port: 3000
  }
};
