import NetworkService from "../services/network.service";
import ApiService from "../services/api.service";

export default {
  install(Vue: any) {
    NetworkService.init();
    Vue.prototype.$api = new ApiService();
  }
};
